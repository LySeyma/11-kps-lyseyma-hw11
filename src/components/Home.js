import React, { Component } from 'react'
import {Card ,Button,InputGroup,FormControl,Form} from 'react-bootstrap';
import CardItem from "./CardItem";
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
          formControls: {
            num1: "",
            num2: "",
            calculator: "plus"
          },
          data: [
            {
              result: ""
            }
          ]
        };
        this.onChange = this.onChange.bind(this);
        this.handleEqualsVal = this.handleEqualsVal.bind(this);
      }
    
      onChange(event) {
        const value = event.target.value;
        const name = event.target.name;
    
        this.setState({
          formControls: {
            ...this.state.formControls,
            [name]: {
              ...this.state.formControls[name],
              value
            }
          }
        });
      }
    
      handleEqualsVal(event) {
        let num1 = this.state.formControls.num1.value;
        let num2 = this.state.formControls.num2.value;
        let result = "";
    
        if(num1 === "" || num2 === "") {
          alert("Cannot calculate number");
        }
    
        else if (isNaN(num1) || isNaN(num2)) {
          alert("Cannot calculate number");
        }
    
        else {
          num1 = Number(num1);
          num2 = Number(num2);
          if (
            this.state.formControls.calculator === "plus" ||
            this.state.formControls.calculator.value === "plus"
          ) {
            result = num1 + num2;
          }
    
          if (this.state.formControls.calculator.value === "subtract") {
            result = num1 - num2;
          }
    
          if (this.state.formControls.calculator.value === "multiple") {
            result = num1 * num2;
          }
    
          if (this.state.formControls.calculator.value === "divide") {
            result = num1 / num2;
          }
    
          if (this.state.formControls.calculator.value === "modulus") {
            result = num1 % num2;
          }
    
          this.setState({
            data: [...this.state.data, { result }]
          });
        }
    
        event.preventDefault();
      }
    
      render() {
        return (
          <div className="container">
            <h1>Wellcome Calculator</h1>
            <div className="row">
              <div className="col-lg-4 col-md-6 col-sm-12">
                <Card className="mt-3">
                  <Card.Img variant="top" src="data:image/webp;base64,UklGRk4PAABXRUJQVlA4WAoAAAAQAAAAKwEAKwEAQUxQSC8HAAABGQVtJCk5epz3b5hfQUT/J0CYiggL/UdXokupTNPAbAZmHAVt2zAJf9j7QxARE4ARp7z0J8FB20aCFP6gx3f3z6EREzABlqhtOyZJup/3DyXLRtso99i2bdu2jeXsbNu27VLb3eWqVER8zyKzBvlHnu8bR8QENPP//P8vke3FZ569Yf2SDDlw+WV/+svB6YHSuPU9NyeyNV38nY9+qzcwGjd8YY/Mnfzea3/SGwxjj99O9g7d4cPPHRsInVdNk8PrX/mmzgAYfyWZ3H7S28dr13rsdC7Bo5/cqplusI18bj/lhqqVms8jp9e/pKUaiVv1soqb31qqD0N3I6879x+ittLi8zKLWywN1eeMlFsnnFUboTPJ7Tg7pHqgWJtdbKhEPSXW5Ne6CNVCSOP5taQKqQ4IOb8UEvUUGS6JWgqR8osIqQ7Icn5JiHqKHJeopyznGKoHMjku/u//+X/+n//n//l//p+koyopp21XQVn/vA0qJ9zwOY2C0njYI1rlhOrp21VOOP35ywoKt3uyCkrnSdtUTlj93MUqJ9ztrhTU4aefonLCpodWKifNh59CQd34pLbKSXXf0wsK6+7fVjnhQaeVlBPvEyonca91BYVTbiuVk+GHNgsKNzxXKiedOzcLCvdYXlLO2CSVk5FbREHhViulcnLi6UE5XXpjqZzEDUMqJmxfEZTT6Xt8W+Wk//x3SMWETz5WlNNf3VUqJ9ffSBTUO4YKylMklZM3S5TTj1RSOfnhqKRi8ttRUU53D0vl5IphCuq+jqRicrQtyul0C5WTfkOomFBJlFNRUG2pnPQSoFIy1QNRSg93KajXTZeUy4rKrsmS8usJcDH57jHApeSDUzbF9I2moD7JdjFJdzG4kOg2LLMopGM3a7aECsmGTdExpfTsZWqolMQtm4FQGWncdCKEKKNLz3dFMT1lWRUqJdV5qQpLZWTsnG6EKKQnLOuHUCE5a6aKEGV0+NRuFUJl5MSqHyEVkm37FCHKaHvtpGYXEa2bCYUoI+0TDigkUURX9HohyUWkseqQkFARWd2bCkkUUa29ioBCMjYxhQCVkGr5VUiijI4fnRKijDbjKlFMFl09A6AiMjyxj2Kq0b/0y0lcOYlxGWlPXAnU4VCOqbe3h6nj1asz7JRVzaqqIqR5d9Wa/GpuajYaESExz63dp0RuDZ85WlWNKoJ5hy9qr8ssjQ9HFSEF893i0K4dmbV8WRVVVQWad5juN2/dzqolSxRVRITmnzE/a23NqdbiBCGJ+W8g9d97j5X51GhUSBGS5h828Pud92rmUrNdSQpJ1AEn6H126G65NJoUoQiJGhqTrIl3nnjXZg51j4ElEPW0ja3uuzp3X5E/vekKIak+ONli8ou77rS5lTkz3T46bk2wnWyp/+ePVDfdvDoyZv/BPmDA1MlOFpF+88O97Y0rVowvIJodkq6rhgeer98fGCGJGtspYUuid+zSS667/vCCIaSoGtGI6hf7NizVYJs4dF1CIGpuk5KNFSCBFgzLtsEBsO4EDbLuXyaNMQMwOSUbSwKxgBpMsgB7atGasYGVfvOHSWxj18/YyckYCWQtGNjY2HJyQmtXVIPpwDf/gOdkEBo7OdkYgQxaIMDGxrNSb3LJCUMDyHs/M7QFMOBBgE2y7YQtA3jBANum3+33+r3uxMTStY1Bs+8rv7hgSyclJ9uDAUyyjW0wYgE12E79lFI/pTTB2sUxSCZ//N0NO5aAZzE4Dck2GOSFBBs7pWQ7OaWpY611IxoU3Z1f7u44q4XsZBsPDDDGxpZZYI2TZxnbPnLt8OqRgeA9n71y25YRjEkeNIDBYBZcY3N844mreqtGVbfpv3z5sm2bFmMDSskmFw0gISEJpSPX9xaPNGrkozu/dnjbpsXhhEFh8lKEFFJICvDBS/cvXlHV5dD3fzi1ZdNy2QYIsPMCBAIBAsHknp3Xj44259/+P3/7d2dvPn+IhA0OESI7kECzAzHbhy7ee6DVbGv+dPfv/cXORVs3L5VtDCACkalCSCAQ4Mkrdv/puunRodA/rt+78s+/u3LorM1rRsDGWKDZZKqQAAwGg83kNb/92b7ekqHFY/+AdN1ll/zpopkTt5yzdgjbmCSHFZLIWDGnMJ4FBi7f+cvf/eaKyRna/aRQFcKpNzNx7NC+q3f99qff769cc+apJ3ZMwhjjAEkim/kNBKA7M3XFpVddfcgYzJwSqFqybt36Ve1mA2ODLYMgCJHHhOSHA4BAyExMTk9Nd3u9hKLRaLU7neEWWMYYjMESIoTIY5IA4fycIxAgBCCOa8Ay2GDMbAmCILv5ew44LMScMggDMmAZy8wtIQmR3RDgAJZlAQhAGIGZ01hGswIhJPKcvwds2SAs/krLAJIlCwkhkfEkgMEAtrA8lwAEIBBCFEFjMBiwzHFlAforSqExGCzzNwtEcTQYDNbxPKt0+nj/BAQAVlA4IPgHAADwRQCdASosASwBPrFYo0okIyQhJPM6IIgWCelu4AQPAyOTpKjPDCvvv5PcS93a6ZQ/HXxPs+5n3AP0z6g/mA87z0X/4z0d+oY5+P2c/3KynPzv+pGQu392g3ZLLmCBsoP8PZFzyPfXe7kXUwAkfSj4ASPpRVmP2mSR3U+SZgyVXnhzKD55v4DTs/5GzpWUsUk8MafTzPwG6ZyzKtjI3CPEgn0UpZjkc+tSj3/jHkiCtFXM3ZOw26P6fT0jSkpSr6dfOoO2KGrpxYFeY4KNS7LA+nq4eMl8L6baWvqAQ9JDp2nyVxUqnRHV6COm4SlUj0PjGOuBkJozc+yeU3sALRd0fkLpHht1SxfYSdn4sCvMtq4YKUoMnM/WLSJDD5elvM5RDKtosxZF3cb0b/oBn46W3q1wpck8XHhgBLx0br6cqr/5y1GjhgQcVOEYW99VdtO6SlNKW3xZ0ln1qR4OvI5JPiKJjhhtz4zBewavWof7epDfwYUhPTF8K/Nlvg1Su74TDfID6z81JsJSJ/SBLUQ4jk8pyLaVCL0CP+Hh2YF3nyMOZBYj8GNqYyc2BTIGkwH8AIOH/Txv66ZUjGBojW+GSKebbEV6ZKp5ghgunbs2lSV9OgyLBUegg+RHUnarBal6lS8FSQMA5ti90C3//erjQF5WcQkR7pcrR+6GQO3Uvww9/wWE7tmkZqIDdwluGy5Jzv3e8YJVIf67edwkqh/b4UXH//k1SuVd+cNuqvGhWW74bC5dudwAAP75M6AAAef8uAWMx1bgIW/ZjQADV0XOK/vSmCDe25JiShQBd3OQIfN9wsT2n0EmTH9xJn0O2/4q8tfaDCyfMVOPfv54RAK+RD7ZrgmWn6XMync2COoQoL9BS1CArFyjXonPDu76Ss4ngtXHJMbbsx3qTckp97Yl23hwrXBN2rCFmI3NQ65kqfardcUzxzHWAAIc2LvgXeKA2iHtW0DTv+ryKuOz3Y7BR0crg57kTXKunmBgKKR6n8sRI8ZxtcI/pcZf0sUeBJXcgvaxsICfF4jr+pcYSC8cuXTF7/Rz6h3cXoupOjTSpNJzSFZoPFeSWTnsP9y2RmiJvMETncD7tObTBflU7ftExadAhMBCmsmuETFxI0wHWHYwPiSoVfhhtMuOe6VxA9egOx2X6O1x6GqZiSRDoALYjUuzY43aOFxkdOxdzg+1sw1MQACGlu8ggZ/rUQAprzOAn3HrFSl+RabViZjIglisAr+QrsdMmODWJWWoykO2KPFMuavbfsydAyIgZSKRq/En/OHXdv3K56iQR5FVl6FniZgf2BmYmHIJs8BxFAF5Zs6xtBEE7+olJDi04gcfwf8FlCSGniMxrqKhqoNTG46QX/OQT4okyCt0ZApy73sNtakVYItN8lf1vcVGmCHUHGZ/TUKGp5aOOUtETtcmQ/vwMpND73DzmxmTttHpQHj5zVo1S8tJcF7eVM0QIVK/F9VVRVnFS7ISAZHOgMCiplKyp+cmVjbTpkDo/eZtDZlc+sVw0Ec73hIkqIm+M+51LYKyqsZfUPoVInhH0AW9KjgYHNVKiDA5qpWcjhXYxfvfp4p1QC8Xgairi0QuAf85RoShFsmSoRrKSNriGCqbyL7G5DX1NemC0xDmk6aJME066WZzo0vQBky42v8xPHMF3Vk1ibFfa2NsKfGaW8CHsNZ0QgQaeMAJS7iNwZwnWUHefT1covo9JzkPdqdiF5OkDk23vAAabtY8qddNQ/hTTPljP8aiIiEO78OcZWhwY40edkJt59ds2Nh3AjZ4BBhXtzi0CQYS1wxzWRS1cIGhmjkjkVgoBZGR8mreoc0rVjxidMDi7hXH4gGK0cfcxwF3jVh0y4NAAVCt7tW7QQlZ3YyiEQOp69KO/ZG05OVzJtNkV6MxwNxhA2cXFF05TqBwzHIpPKz7OZWyKsCF3EDpv/GfG5TGQ6wsAm1jgvr/j8YwMCKQgpb/c1RX+5eH6v/ii+YW6CdDJq9j2Q82a6EhSS8AcXMPXP4euJBdMQkdrxiPMc29l3ddWrjme3hNzlGneGD0Ayn9dyQ1vOAzREE/3nhqXfjdZtJ/I+rVr3lIkmJVe4hfjHOLXURkM+BTUuuhN85C9KSGq4v52ugwhbVSyXLWdN1pfFfFksRd6mlnh0+pMnMtTDTfnLUcmdiObOqh2nwHlK6FrDg0TtwOO9dQu0wUMl3vU254VaqVqJfeVOknxaBUHmjbCFjCgPIw0sn/+zr1iWqZzfULztNp+WZu72NNa2ful4226HWVgDtdURzkwtfFTHLzg4jci0W8PVY/OT/eXwHxuGkt59uYPiHpbptXfP+cHTz+8m0UJ6NTXpOpaDD9qfUDMwmbd8EcBjnWiZj1LX/FBUWxTiIl2a0lf7B5UlzbCJnPKl2XpIC9N6jed08milNvFZnDGweIKRzduBiOcpaTjTFxvl7nj12vFOcOfEEjVJZlErUYxP6jVKDvRVTRjBr/cowCecAkRbXJkP1cJHamfwTqGB88HEUvKbhhrf4J/OvboV5n7ieFL2uZO/Oten1ba4QiDhPdmcyENTW5d7xmjDuO4qVjIlEQVdlbNbxLbzIB4G5H04x2JQSwHCnaPDVYKhUIryXzwm9rQZjxwJGdP9oW6v0ldOcF26tVU3M9NrfVgZDdc4Av7mo3U7mJoPdIr9LQ/LFGAe9t2QPUAAL1/BDirMtvCtEXvQUAamsc+9/wAAA=" />
                  <Card.Body>
                    <Form noValidate onSubmit={this.handleEqualsVal}>
                      <Form.Group>
                        <Form.Control
                          name="num1"
                          placeholder="Number "
                          type="number"
                          value={this.state.formControls.num1.value}
                          onChange={this.onChange}
                        />
                      </Form.Group>
    
                      <Form.Group>
                        <Form.Control
                          name="num2"
                          placeholder="Number "
                          type="number"
                          onChange={this.onChange}
                          value={this.state.formControls.num2.value}
                        />
                      </Form.Group>
    
                      <Form.Group>
                        <Form.Control
                          as="select"
                          name="calculator"
                          className="text-primary"
                          value={this.state.formControls.calculator.value}
                          onChange={this.onChange}
                        >
                          <option value="plus">+ Plus</option>
                          <option value="subtract">- Subtract</option>
                          <option value="multiple">* Multiple</option>
                          <option value="divide">/ Divide</option>
                          <option value="modulus">% Modulus</option>
                        </Form.Control>
                      </Form.Group>
                      <Button variant="primary" type="submit">
                        Calculate
                      </Button>
                    </Form>
                  </Card.Body>
                </Card>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-12">
                <h2 className="mb-3">Result History</h2>
                {this.state.data.map((value, index) => {
                  if (value.result.length === 0) {
                    return "";
                  } else {
                    return <CardItem key={index} result={value.result} />;    
                  }  
                })}
              </div>
            </div>
          </div>
        );
      }
    
}

