import React, { Component } from 'react'

export default class CardItem extends Component {
    render() {
        return (
            <div className="card">
        <p className="card-text p-2" key={this.props.index}>
          {this.props.result}
        </p>
      </div>

        );
    }
}
